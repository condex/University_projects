library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;		 


entity memoria is
port(	
		SRAM_ADDR: in std_logic_vector(7 downto 0);
		SRAM_DQ: inout STD_LOGIC_VECTOR(0 to 15);
		SRAM_WE_N: in std_logic;
		SRAM_OE_N: in std_logic;
		SRAM_UB_N: in std_logic;
		SRAM_LB_N: in std_logic;
		SRAM_CE_N: in std_logic
);
end memoria ;



architecture behavior of  memoria  is

    type mem_data_type is array (0 to 255) 
	of STD_LOGIC_VECTOR(0 to 15);
	signal mem_data : mem_data_type;
begin
    process(SRAM_ADDR,SRAM_WE_N,SRAM_OE_N,SRAM_UB_N,SRAM_LB_N,SRAM_CE_N,SRAM_DQ)
    begin
    SRAM_DQ <= (OTHERS => 'Z');
		 if SRAM_CE_N='0' then
			if SRAM_WE_N='1' then
				if SRAM_OE_N='0' then
					SRAM_DQ <= mem_data(to_integer(unsigned(SRAM_ADDR)));
				else
					SRAM_DQ <= (others=>'Z');
				end if;
			else 
					mem_data(to_integer(unsigned(SRAM_ADDR)))<=SRAM_DQ;
			end if;
		else
			SRAM_DQ <= (others=>'Z');
		end if;
    end process;
end behavior;
