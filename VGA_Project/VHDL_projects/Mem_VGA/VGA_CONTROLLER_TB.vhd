LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE STD.TEXTIO.ALL;

ENTITY VGA_CONTROLLER_TB IS
END VGA_CONTROLLER_TB ;

ARCHITECTURE STRUCTURE OF VGA_CONTROLLER_TB IS


COMPONENT VGA_CONTROLLER IS
PORT( 	CLOCK_50:IN STD_LOGIC;
			SW : IN STD_LOGIC_VECTOR(0 TO 17);
			RGB 	: IN STD_LOGIC_VECTOR(29 DOWNTO 0);
			--R,G,B	: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
			REQ     : OUT STD_LOGIC;
			--ACK     : IN STD_LOGIC;
			VGA_CLOCK,VGA_SYNC,VGA_BLANK,VGA_VS,
			VGA_HS	: OUT STD_LOGIC;

			VGA_R :OUT STD_LOGIC_VECTOR(0 TO 9);
			VGA_G :OUT STD_LOGIC_VECTOR(0 TO 9);
			VGA_B :OUT STD_LOGIC_VECTOR(0 TO 9);
			X       : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
			Y       : OUT STD_LOGIC_VECTOR(8 DOWNTO 0)
	);                      
END COMPONENT;

--COMPONENT microrom is
--port(	
--		X: in std_logic_vector(9 downto 0);
--		Y: in std_logic_vector(8 downto 0);
--		CLOCK,REQ,RESETN: in STD_LOGIC;
--		ACK: OUT STD_LOGIC;
--		DATA_OUT: out STD_LOGIC_VECTOR(29 downto 0)
--);
--END COMPONENT;

SIGNAL CLK,RESETN:STD_LOGIC;
SIGNAL RGB  	:STD_LOGIC_VECTOR(29 DOWNTO 0);
SIGNAL R,G,B	:STD_LOGIC_VECTOR(9 DOWNTO 0);
SIGNAL REQ      :STD_LOGIC;
SIGNAL ACK      :STD_LOGIC;
SIGNAL VGA_CLOCK,VGA_SYNC,VGA_BLANK,VGA_VS,VGA_HS	:STD_LOGIC;
SIGNAL VGA_R,VGA_G,VGA_B: STD_LOGIC_VECTOR(0 TO 9);
SIGNAL X        :STD_LOGIC_VECTOR(9 DOWNTO 0);
SIGNAL Y        :STD_LOGIC_VECTOR(8 DOWNTO 0);
SIGNAL SW       :STD_LOGIC_VECTOR(0 TO 17);

BEGIN

CLKSYST: PROCESS
BEGIN
CLK <= '1';
WAIT FOR 10 ns;
CLK <= '0';
WAIT FOR 10 ns;
END PROCESS;

Reset: PROCESS
BEGIN
RESETN <= '0';
WAIT FOR 5 ns;
RESETN <= '1';
WAIT;
END PROCESS;

ack_proc: PROCESS
BEGIN
ACK<='0';
WAIT FOR 40 ns;
ACK<='1';
WAIT;
END PROCESS;

--DATIPROVA: PROCESS(CLK,RESETN,REQ)
	    
			
--			FILE FILE_OPENED: TEXT IS IN "DATI.dat";
--			VARIABLE LINE_READ: LINE;
--			VARIABLE DAT : integer;
--			VARIABLE CONT:INTEGER;
--			begin
--			
--				 IF CLK' EVENT AND CLK='1' THEN
--					IF RESETN='0' THEN
--					ACK<='0';
--					CONT:=0;
--					ELSE
--						IF REQ='0' THEN
--							CONT:=0;
--							ACK<='0';
--						ELSE
--							CONT:=CONT+1;
--							IF CONT>=1 THEN
--								ACK<='1';
--								IF endfile( FILE_OPENED ) then
--								ELSE
--								READLINE( FILE_OPENED, LINE_READ);
--								READ( LINE_READ, DAT );
--								RGB <= std_logic_vector(to_signed(DAT ,30));
--								END IF;
--							ELSE
--							END IF;
--						END IF;		
--					END IF;	
--				 END IF;
--			
--END PROCESS;
SW(0)<=RESETN;
SW(1)<=ACK;
SW(2 TO 17)<=(others =>'1');
 
DUT: VGA_CONTROLLER  port map ( CLOCK_50=>CLK,SW=>SW,RGB=>RGB,
	    REQ=>REQ,VGA_CLOCK=>VGA_CLOCK,VGA_SYNC=>VGA_SYNC,
	    VGA_BLANK=>VGA_BLANK,VGA_VS=>VGA_VS,VGA_HS=>VGA_HS,X=>X,Y=>Y,VGA_R=>VGA_R,VGA_G=>VGA_G,VGA_B=>VGA_B);
--DATI_PROVA:  microrom PORT MAP(X=>X,Y=>Y,CLOCK=>CLK,RESETN=>RESETN,ACK=>ACK,DATA_OUT=>RGB,REQ=>REQ);
     
     
END STRUCTURE;
