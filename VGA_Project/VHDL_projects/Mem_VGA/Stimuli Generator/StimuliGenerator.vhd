-- Stimuli Generator
-- Macchina che fornisci gli stimoli per testare il "Memory Controller"

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL; 

ENTITY StimuliGenerator IS 
PORT (-- Ingressi
	  CLOCK_50: IN STD_LOGIC;
	  Start: IN STD_LOGIC;
	  ResetN: in std_logic;
	  -- Uscite
	  DoneS: OUT STD_LOGIC;
	  Data: OUT STD_LOGIC_VECTOR(29 DOWNTO 0);
	  X: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
	  Y: OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
	  WR: OUT STD_LOGIC);
END StimuliGenerator;

ARCHITECTURE Structure OF StimuliGenerator IS

COMPONENT Contatore_Modulo_N IS
GENERIC (N: POSITIVE := 10;
		 M: POSITIVE := 4);
PORT (Data_Input: IN STD_LOGIC_VECTOR(M-1 downto 0);   --Dato caricabile in ingresso da cui cominciare a contare
	  LD: IN STD_LOGIC;                                    --Load: se asserito carica nel contatore Data_Input
	  CE: IN STD_LOGIC;                                    --Counter Enable: se asserito abilita il contatore a contare
	  INC_DECN: IN STD_LOGIC;							   --Incrementa o decrementa inc=1 dec=0;
	  A_RST_n: IN STD_LOGIC;                               --Reset asincrono attivo basso
	  CLK: IN STD_LOGIC;                                   --Clock di sistema
	  Data_Output: BUFFER STD_LOGIC_VECTOR(M-1 downto 0); 
	  TC: OUT STD_LOGIC);                                 --Terminal Count: viene asserito quando Data_Output = N-1
END COMPONENT;

TYPE StateType IS (Idle, WR_E, NO_Y, SI_Y, Done);
SIGNAL State: StateType;
SIGNAL CE_256, CE_X, CE_Y, Clock: STD_LOGIC;
SIGNAL TC_256, TC_X, TC_Y: STD_LOGIC;
SIGNAL Data_256: STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL Data_X: STD_LOGIC_VECTOR(4 DOWNTO 0);
SIGNAL Data_Y: STD_LOGIC_VECTOR(2 DOWNTO 0);

BEGIN

ckdiv: PROCESS (CLOCK_50, ResetN)                  -- Divisore di frequenza. Freq in uscita 25MHz
BEGIN 
IF ResetN = '0' THEN                               -- Asynchronous reset (active low)
	Clock <= '0';
    ELSIF CLOCK_50' EVENT AND CLOCK_50 = '1' THEN  -- Rising clock edge
		Clock <= NOT Clock;
    END IF;
END PROCESS ckdiv;

Transition: PROCESS(Clock, ResetN) 
BEGIN
IF ResetN = '0' THEN  State <= Idle;
ELSIF (CLOCK 'EVENT AND CLOCK = '1') THEN 
	CASE State IS
	WHEN Idle => IF Start = '1' THEN State <= WR_E; ELSE State <= Idle; END IF;
	WHEN WR_E => State <= NO_Y;
	WHEN NO_Y => IF Data_X = "11110" THEN State <= SI_Y; ELSE State <= NO_Y; END IF;
	WHEN SI_Y => IF Data_Y = "111" THEN IF TC_X = '1' THEN State <= Done; ELSE State <= NO_Y; END IF; ELSE State <= NO_Y; END IF;
	WHEN Done => IF Start = '0' THEN State <= Idle; ELSE State <= Done; END IF;
	END CASE;
END IF;
END PROCESS;

ASM_Output: PROCESS(State)
BEGIN
WR <= '0';
CE_256 <= '0';
CE_X <= '0';
CE_Y <= '0';
DoneS <= '0';
CASE State IS
WHEN Idle => 
WHEN WR_E => WR <= '1';
WHEN NO_Y => CE_256 <= '1';
			 CE_X <= '1';
			 WR <= '1';
WHEN SI_Y => CE_256 <= '1';
			 CE_X <= '1';
			 CE_Y <= '1';
			 WR <= '1';
WHEN Done => DoneS <= '1';
END CASE;
END PROCESS;

Contatore_256: Contatore_Modulo_N GENERIC MAP(M => 8, N => 256)
								   PORT MAP(CLK => Clock, A_RST_n => ResetN, LD => '0', Data_Input => "00000000", CE => CE_256, Data_Output => Data_256, TC => TC_256, INC_DECN => '1');
Contatore_X: Contatore_Modulo_N GENERIC MAP(M => 5, N => 32)
								PORT MAP(CLK => Clock, A_RST_n => ResetN, LD => '0', Data_Input => "00000", CE => CE_X, Data_Output => Data_X, TC => TC_X, INC_DECN => '1');
Contatore_Y: Contatore_Modulo_N GENERIC MAP(M => 3, N => 8)
								PORT MAP(CLK => Clock, A_RST_n => ResetN, LD => '0', Data_Input => "000", CE => CE_Y, Data_Output => Data_Y, TC => TC_Y, INC_DECN => '1');
X(9 DOWNTO 5) <= "00000";
X(4 DOWNTO 0) <= Data_X;
Y(8 DOWNTO 3) <= "000000";
Y(2 DOWNTO 0) <= Data_Y;
Data(29 DOWNTO 27) <= Data_256(7 downto 5);
Data(19 DOWNTO 17) <= Data_256(4 downto 2);
Data(9 DOWNTO 8) <= Data_256(1 downto 0);
Data(26 DOWNTO 20) <= "0000000";
Data(16 DOWNTO 10) <= "0000000";
Data(7 DOWNTO 0) <= "00000000";

END Structure;