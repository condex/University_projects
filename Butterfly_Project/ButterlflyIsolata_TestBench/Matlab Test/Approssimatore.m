function [Output] = Approssimatore(Input, Nbit_Ap)
    Nbit = length(Input);
    if(Input(Nbit_Ap + 1) == '1')
        if(strcmp (Input(Nbit_Ap + 1:Nbit), '100000000000000000') == 1)
            if(Input(Nbit_Ap) == '1')
                Output = dec2bin(bin2dec(Input(1:Nbit_Ap)) + bin2dec('1'), Nbit_Ap);
            else
                Output = Input(1:Nbit_Ap);
            end
        else
            Output = dec2bin(bin2dec(Input(1:Nbit_Ap)) + bin2dec('1'), Nbit_Ap);
        end
    else
        Output = Input(1:Nbit_Ap);
    end
end

