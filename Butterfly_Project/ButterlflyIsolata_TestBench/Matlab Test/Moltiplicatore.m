% Acquisisce due codici binari di egual lunghezza e ne restituisce il
% prodotto (Shift_MolN = 0) o lo schift di In1 (SShift_MolN = 1) in codice binario di 
% lunghezza doppia di quella degli operandi

function [Output] = Moltiplicatore(In1, In2, Shift_MolN)
    m = length(In1);
    In1_Dec = bin2dec_n(In1);
    In2_Dec = bin2dec_n(In2);
    if Shift_MolN == 0
        Output_Dec = In1_Dec * In2_Dec;
    else
        Output_Dec = 2*In1_Dec;
    end
    Output = dec2bin_n(Output_Dec, 2*m);
end

