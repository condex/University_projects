% Acquisisce due codici binari di egual lunghezza e ne restituisce la somma
% (Sub_AddN = 0) o la differenza (Sub_AddN = 1) in codice binari di egual
% lunghezza degli operandi

function [Output,n] = Sommatore(In1, In2, Sub_AddN)
    m = zeros(1,2);
    m(1) = length(In1);
    m(2) = length(In2);
    n = max(m);
    In1_Dec = bin2dec_n(In1);
    In2_Dec = bin2dec_n(In2);
    if Sub_AddN == 0
        Output_Dec = In1_Dec + In2_Dec;
    else
        Output_Dec = In1_Dec - In2_Dec;
    end
    Output = dec2bin_n(Output_Dec, 34);
end

