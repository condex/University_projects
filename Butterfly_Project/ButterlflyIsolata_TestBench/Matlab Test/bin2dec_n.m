% Converte in valore decimale codici binari anche negativi

function [Decimale] = bin2dec_n(Binario)
    Decimale = 0;
    m = length(Binario);
    for i=1:m
        if i==1
            if Binario(i) == '1'
                Decimale = -2^(m-i);
            end
        else
            if Binario(i) == '1'
                Decimale = Decimale + 2^(m-i);
            end
        end
    end
end

