% Test Butterlfy verisone 3

close all
clear all
clc

text1 = fopen('Data_In.txt', 'w');
text2 = fopen('Data_Out.txt', 'w');

format long

%%%%% Input %%%%%
Nbit_Input = 16;
Bit_Guardia = 2; % Numero di bit di guarda che si desidera inserire nel codice
Nbit = Nbit_Input - Bit_Guardia;
M = 1; % Numeri di codici che si desidera generare

%%%% Generazione Input %%%%
MaxValue = 2^(Nbit) - 1;

for a=1:M
%Wr
DataGeneratorWr = randi([0, MaxValue], 1, 1);        % La funzione genera solo numeri positivi
InDecimale_Wr = (DataGeneratorWr - 2^(Nbit_Input-1-Bit_Guardia));
%Wi
DataGeneratorWi = randi([0, MaxValue], 1, 1);        % La funzione genera solo numeri positivi
InDecimale_Wi = (DataGeneratorWi - 2^(Nbit_Input-1-Bit_Guardia));
%Ar
DataGeneratorAr = randi([0, MaxValue], 1, 1);        % La funzione genera solo numeri positivi
InDecimale_Ar = (DataGeneratorAr - 2^(Nbit_Input-1-Bit_Guardia));
%Ai
DataGeneratorAi = randi([0, MaxValue], 1, 1);        % La funzione genera solo numeri positivi
InDecimale_Ai = (DataGeneratorAi - 2^(Nbit_Input-1-Bit_Guardia));
%Br
DataGeneratorBr = randi([0, MaxValue], 1, 1);        % La funzione genera solo numeri positivi
InDecimale_Br = (DataGeneratorBr - 2^(Nbit_Input-1-Bit_Guardia));
%Bi
DataGeneratorBi = randi([0, MaxValue], 1, 1);        % La funzione genera solo numeri positivi
InDecimale_Bi = (DataGeneratorBi - 2^(Nbit_Input-1-Bit_Guardia));

InBinario_Br(a,:) = dec2bin_n(InDecimale_Br, Nbit_Input);
InBinario_Bi(a,:) = dec2bin_n(InDecimale_Bi, Nbit_Input);
InBinario_Ar(a,:) = dec2bin_n(InDecimale_Ar, Nbit_Input);
InBinario_Ai(a,:) = dec2bin_n(InDecimale_Ai, Nbit_Input);
InBinario_Wr(a,:) = dec2bin_n(InDecimale_Wr, Nbit_Input);
InBinario_Wi(a,:) = dec2bin_n(InDecimale_Wi, Nbit_Input);

M1 = Moltiplicatore(InBinario_Br(a,:), InBinario_Wr(a,:), 0);
M2 = Moltiplicatore(InBinario_Bi(a,:), InBinario_Wi(a,:), 0);
M3 = Moltiplicatore(InBinario_Br(a,:), InBinario_Wi(a,:), 0);
M4 = Moltiplicatore(InBinario_Bi(a,:), InBinario_Wr(a,:), 0);
M5 = Moltiplicatore(InBinario_Ar(a,:), InBinario_Wr(a,:), 1);
M6 = Moltiplicatore(InBinario_Ai(a,:), InBinario_Wr(a,:), 1);
S1 = Sommatore(InBinario_Ar(a,:), M1, 1);
S2 = Sommatore(S1, M2, 0);
S3 = Sommatore(InBinario_Ai(a,:), M3, 1);
S4 = Sommatore(S3, M4, 1);
S5 = Sommatore(M5, S2, 1);
S6 = Sommatore(M6, S4, 1);

OutBinario_Br(a,:) = Approssimatore(S2, 16);
OutBinario_Bi(a,:) = Approssimatore(S4, 16);
OutBinario_Ar(a,:) = Approssimatore(S5, 16);
OutBinario_Ai(a,:) = Approssimatore(S6, 16);

end

%%%% Stampa su File %%%%

T = 100; %Periodo del clock in us

for n=1:M
    if (n<M)
        if(n==1)
            %Wi
            fprintf(text1, '"');
            fprintf(text1, InBinario_Wi(n,:));
            fprintf(text1, '", ');
            %Wr
            fprintf(text1, '"');
            fprintf(text1, InBinario_Wr(n,:));
            fprintf(text1, '" after ');
            fprintf(text1, '%3d', 2*T*n+50);
            fprintf(text1, ' us, ');
            %Br
            fprintf(text1, '"');
            fprintf(text1, InBinario_Br(n,:));
            fprintf(text1, '" after ');
            fprintf(text1, '%3d', (3*T)*n+50);
            fprintf(text1, ' us, ');
            %Bi
            fprintf(text1, '"');
            fprintf(text1, InBinario_Bi(n,:));
            fprintf(text1, '" after ');
            fprintf(text1, '%3d', (4*T)*n+50);
            fprintf(text1, ' us, ');
            %Ar
            fprintf(text1, '"');
            fprintf(text1, InBinario_Ar(n,:));
            fprintf(text1, '" after ');
            fprintf(text1, '%3d', (5*T)*n+50);
            fprintf(text1, ' us, ');
            %Ai
            fprintf(text1, '"');
            fprintf(text1, InBinario_Ai(n,:));
            fprintf(text1, '" after ');
            fprintf(text1, '%3d', (6*T)*n+50);
            fprintf(text1, ' us, ');
            %Br_O
            fprintf(text2, '"');
            fprintf(text2, OutBinario_Br(n,:));
            fprintf(text2, '" after ');
            fprintf(text2, '%3d', (10*T)*n);
            fprintf(text2, ' us, ');
            %Bi_O
            fprintf(text2, '"');
            fprintf(text2, OutBinario_Bi(n,:));
            fprintf(text2, '" after ');
            fprintf(text2, '%3d', (11*T)*n);
            fprintf(text2, ' us, ');
            %Ar_O
            fprintf(text2, '"');
            fprintf(text2, OutBinario_Ar(n,:));
            fprintf(text2, '" after ');
            fprintf(text2, '%3d', (12*T)*n);
            fprintf(text2, ' us, ');
            %Ai_O
            fprintf(text2, '"');
            fprintf(text2, OutBinario_Ai(n,:));
            fprintf(text2, '" after ');
            fprintf(text2, '%3d', (13*T)*n);
            fprintf(text2, ' us, ');
        else
            %Wi
            fprintf(text1, '"');
            fprintf(text1, InBinario_Wi(n,:));
            fprintf(text1, '" after ');
            fprintf(text1, '%3d', (14*T)+(14*T*(n-2))+150);
            fprintf(text1, ' us, ');
            %Wr
            fprintf(text1, '"');
            fprintf(text1, InBinario_Wr(n,:));
            fprintf(text1, '" after ');
            fprintf(text1, '%3d', (15*T)+(14*T*(n-2))+150);
            fprintf(text1, ' us, ');
            %Br
            fprintf(text1, '"');
            fprintf(text1, InBinario_Br(n,:));
            fprintf(text1, '" after ');
            fprintf(text1, '%3d', (16*T)+(14*T*(n-2))+150);
            fprintf(text1, ' us, ');
            %Bi
            fprintf(text1, '"');
            fprintf(text1, InBinario_Bi(n,:));
            fprintf(text1, '" after ');
            fprintf(text1, '%3d', (17*T)+(14*T*(n-2))+150);
            fprintf(text1, ' us, ');
            %Ar
            fprintf(text1, '"');
            fprintf(text1, InBinario_Ar(n,:));
            fprintf(text1, '" after ');
            fprintf(text1, '%3d', (18*T)+(14*T*(n-2))+150);
            fprintf(text1, ' us, ');
            %Ai
            fprintf(text1, '"');
            fprintf(text1, InBinario_Ai(n,:));
            fprintf(text1, '" after ');
            fprintf(text1, '%3d', (19*T)+(14*T*(n-2))+150);
            fprintf(text1, ' us, ');
            %Br_O
            fprintf(text2, '"');
            fprintf(text2, OutBinario_Br(n,:));
            fprintf(text2, '" after ');
            fprintf(text2, '%3d', (23*T)+(14*T*(n-2))+100);
            fprintf(text2, ' us, ');
            %Bi_O
            fprintf(text2, '"');
            fprintf(text2, OutBinario_Bi(n,:));
            fprintf(text2, '" after ');
            fprintf(text2, '%3d', (24*T)+(14*T*(n-2))+100);
            fprintf(text2, ' us, ');
            %Ar_O
            fprintf(text2, '"');
            fprintf(text2, OutBinario_Ar(n,:));
            fprintf(text2, '" after ');
            fprintf(text2, '%3d', (25*T)+(14*T*(n-2))+100);
            fprintf(text2, ' us, ');
            %Ai_O
            fprintf(text2, '"');
            fprintf(text2, OutBinario_Ai(n,:));
            fprintf(text2, '" after ');
            fprintf(text2, '%3d', (26*T)+(14*T*(n-2))+100);
            fprintf(text2, ' us, ');
        end
    else
        %Wi
            fprintf(text1, '"');
            fprintf(text1, InBinario_Wi(n,:));
            fprintf(text1, '" after ');
            fprintf(text1, '%3d', (14*T)+(14*T*(n-2))+150);
            fprintf(text1, ' us, ');
            %Wr
            fprintf(text1, '"');
            fprintf(text1, InBinario_Wr(n,:));
            fprintf(text1, '" after ');
            fprintf(text1, '%3d', (15*T)+(14*T*(n-2))+150);
            fprintf(text1, ' us, ');
            %Br
            fprintf(text1, '"');
            fprintf(text1, InBinario_Br(n,:));
            fprintf(text1, '" after ');
            fprintf(text1, '%3d', (16*T)+(14*T*(n-2))+150);
            fprintf(text1, ' us, ');
            %Bi
            fprintf(text1, '"');
            fprintf(text1, InBinario_Bi(n,:));
            fprintf(text1, '" after ');
            fprintf(text1, '%3d', (17*T)+(14*T*(n-2))+150);
            fprintf(text1, ' us, ');
            %Ar
            fprintf(text1, '"');
            fprintf(text1, InBinario_Ar(n,:));
            fprintf(text1, '" after ');
            fprintf(text1, '%3d', (18*T)+(14*T*(n-2))+150);
            fprintf(text1, ' us, ');
            %Ai
            fprintf(text1, '"');
            fprintf(text1, InBinario_Ai(n,:));
            fprintf(text1, '" after ');
            fprintf(text1, '%3d', (19*T)+(14*T*(n-2))+150);
            fprintf(text1, ' us;');
            %Br_O
            fprintf(text2, '"');
            fprintf(text2, OutBinario_Br(n,:));
            fprintf(text2, '" after ');
            fprintf(text2, '%3d', (23*T)+(14*T*(n-2))+100);
            fprintf(text2, ' us, ');
            %Bi_O
            fprintf(text2, '"');
            fprintf(text2, OutBinario_Bi(n,:));
            fprintf(text2, '" after ');
            fprintf(text2, '%3d', (24*T)+(14*T*(n-2))+100);
            fprintf(text2, ' us, ');
            %Ar_O
            fprintf(text2, '"');
            fprintf(text2, OutBinario_Ar(n,:));
            fprintf(text2, '" after ');
            fprintf(text2, '%3d', (25*T)+(14*T*(n-2))+100);
            fprintf(text2, ' us, ');
            %Ai_O
            fprintf(text2, '"');
            fprintf(text2, OutBinario_Ai(n,:));
            fprintf(text2, '" after ');
            fprintf(text2, '%3d', (26*T)+(14*T*(n-2))+100);
            fprintf(text2, ' us;');
    end
end

fclose(text1);
fclose(text2);