function [Ar_O, Ai_O, Br_O, Bi_O] = Butterfly(Wr, Wi, Ar, Ai, Br, Bi)
    Br_O = Ar - Br.*Wr + Bi.*Wi;
    Bi_O = Ai - Br.*Wi - Bi.*Wr;
    Ar_O = 2*Ar - Br_O;
    Ai_O = 2*Ai - Bi_O;
end

