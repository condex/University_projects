function [Ca2Binary, flag, Out_Temp, m] = CA2(Binary)
    m = length(Binary);
    for i=1:m
        if(Binary(i) == '1')
            Binary(i) = '0';
        else
            Binary(i) = '1';
        end
    end
    flag = 0;
    Out_Temp = dec2bin(bin2dec(Binary) + bin2dec('1'), m); %Uscita temporanea
    Ca2BinaryInitialize = zeros(1);
    Ca2Binary = dec2bin(Ca2BinaryInitialize, m);
    if (length(Out_Temp) == length(Binary)+1)

    else
        Ca2Binary = Out_Temp;
    end 
end

