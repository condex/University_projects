% Test

clear all 
clc

format long

MaxValue = 8191;
MinValue = -8192;

BrMax = MaxValue - (MaxValue*MinValue) + (MaxValue*MaxValue)
BrMin = MinValue - (MinValue*MinValue) + (MinValue*MaxValue)
BiMax = MaxValue - (MaxValue*MinValue) - (MinValue*MaxValue)
BiMin = MinValue - (MinValue*MinValue) - (MinValue*MinValue)
ArMax = 2*MaxValue - BrMin
ArMin = 2*MinValue - BrMax
AiMax = 2*MaxValue - BiMin
AiMin = 2*MinValue - BiMax

Bit = zeros(1,4);
if(BrMax > BiMax)
    Bit(1) = log2(BrMax);
else
    Bit(1) = log2(BiMax);
end

if(BrMin > BiMin)
    Bit(2) = log2(-BrMin);
else
    Bit(2) = log2(-BiMin);
end

if(ArMax > AiMax)
    Bit(3) = log2(ArMax)
else
    Bit(3) = log2(AiMax)
end

if(ArMin > AiMin)
    Bit(4) = log2(-ArMin)
else
    Bit(4) = log2(-AiMin)
end

NBit = max(Bit) 

MaxValue_f = 8191*2^-15;
MinValue_f = -8192*2^-15;

BrMax_f = MaxValue_f - (MaxValue_f*MinValue_f) + (MaxValue_f*MaxValue_f)
BrMin_f = MinValue_f - (MinValue_f*MinValue_f) + (MinValue_f*MaxValue_f)
BiMax_f = MaxValue_f - (MaxValue_f*MinValue_f) - (MinValue_f*MaxValue_f)
BiMin_f = MinValue_f - (MinValue_f*MinValue_f) - (MinValue_f*MinValue_f)
ArMax_f = 2*MaxValue_f - BrMin_f
ArMin_f = 2*MinValue_f - BrMax_f
AiMax_f = 2*MaxValue_f - BiMin_f
AiMin_f = 2*MinValue_f - BiMax_f

K_Out = zeros(1,8);

K_Out(1) = BrMax_f/BrMax;
K_Out(2) = BrMin_f/BrMin;
K_Out(3) = BiMax_f/BiMax;
K_Out(4) = BiMin_f/BiMin;
K_Out(5) = ArMax_f/ArMax;
K_Out(6) = ArMin_f/ArMin;
K_Out(7) = AiMax_f/AiMax;
K_Out(8) = AiMin_f/AiMin;

max(K_Out)
OutK = log2(max(K_Out))
            