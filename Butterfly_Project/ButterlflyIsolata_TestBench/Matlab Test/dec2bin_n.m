% Cobverte in codici binari valori decimale anche negativi 

function [Binario] = dec2bin_n(Decimale, Nbit)
    Binario = dec2bin(0, Nbit);
    if(Decimale > 0)
        Binario = dec2bin(Decimale, Nbit);
    else
        Binario = CA2(dec2bin(-Decimale, Nbit));
    end
end

