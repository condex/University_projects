LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
ENTITY uir IS
PORT (R : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
LOAD: IN STD_LOGIC;
ClOCK, RESETN : IN STD_LOGIC;
Q : OUT STD_LOGIC_VECTOR(23 DOWNTO 0));
END uir;
ARCHITECTURE Behavior OF uir IS
BEGIN
PROCESS (CLOCK, RESETN)
BEGIN
IF (RESETN = '0') THEN
Q <= "100000000000000000000000";
ELSIF (CLOCK'EVENT AND CLOCK = '0') THEN
IF LOAD='1' THEN
Q <= R;
END IF;
END IF;
END PROCESS;
END Behavior;