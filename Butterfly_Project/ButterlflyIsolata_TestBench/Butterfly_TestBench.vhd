-- Test Bench per la Butterfly

LIBRARY IEEE;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE STD.TEXTIO.ALL;

ENTITY Butterfly_TestBench IS

END Butterfly_TestBench;

ARCHITECTURE structure OF Butterfly_TestBench IS

COMPONENT butterfly is
port(DATA_IN: IN SIGNED(15 DOWNTO 0);
	 START,CLK,RESETN : IN STD_LOGIC;
	 DATA_OUT: OUT SIGNED(15 DOWNTO 0);
	 DONE : OUT STD_LOGIC
	);
END COMPONENT; 

SIGNAL DATA_IN: SIGNED (15 DOWNTO 0) := (Others => '0');
SIGNAL DATA_OUT: SIGNED (15 DOWNTO 0);
SIGNAL Checker: SIGNED (15 DOWNTO 0) := (Others => '0');
SIGNAL DONE, CLK, RESETN, Er1, Er2, Errore, START: STD_LOGIC;


BEGIN

CLKSYST: PROCESS
BEGIN
CLK <= '0';
WAIT FOR 50 us;
CLK <= '1';
WAIT FOR 50 us;
END PROCESS;

Reset: PROCESS
BEGIN
RESETN <= '0';
WAIT FOR 20 us;
RESETN <= '1';
WAIT;
END PROCESS;

Analisi: PROCESS
BEGIN
START <= '1', '0' AFTER 300 us;
--DATA_IN <= "0000001100000000" after 150 us, "1111000111010010" after 255 us, "1110101000010110" after 355 us, "0001111000011110" after 455 us, "0001110101000111" after 555 us, "0001110111000000" after 655 us;
--Checker <= "1111111111001001" after 1000 us, "0000000001111011" after 1100 us, "0000000000110111" after 1200 us, "1111111110000101" after 1300 us;
--Er1 <= '0', '1' after 1000 us, '0' after 1400 us, '1' after 2400 us, '0' after 2800 us, '1' after 3800 us, '0' after 4200 us, '1' after 5200 us, '0' after 5600 us, '1' after 6600 us, '0' after 7000 us, '1' after 8000 us, '0' after 8400 us, '1' after 9400 us, '0' after 9800 us, '1' after 10800 us, '0' after 11200 us, '1' after 12200 us, '0' after 12600 us, '1' after 13600 us, '0' after 14000 us;
WAIT;
END PROCESS;

LETTURADAIIN: PROCESS(CLK,RESETN)

			FILE FILE_OPENED: TEXT IS IN "DATIIN.dat";
			VARIABLE LINE_READ: LINE;
			VARIABLE DAT : integer;
			VARIABLE CONT:INTEGER;
			begin
			   
			   IF RESETN='0' THEN
					--START<='0';
					CONT:=0;
					
				 ELSIF CLK' EVENT AND CLK='1' THEN
					CONT:=CONT+1;
					if cont = 1 then
					--START<='1';
					end if;
					if cont>12 then
					  cont:=0;
					  
					  else
						IF CONT>1 AND CONT<8 THEN
								IF endfile( FILE_OPENED ) then
								ELSE
								READLINE( FILE_OPENED, LINE_READ);
								READ( LINE_READ, DAT );
								DATA_IN <= to_signed(DAT ,16);
								END IF;
								IF CONT>=7 THEN
									--START<='0';
								END IF;
							
					END IF;	
					END IF;	
					END IF;	
			
END PROCESS;

LETTURADATICECKER: PROCESS(CLK,RESETN)

			FILE FILE_OPENEDC: TEXT IS IN "DATICECKER.dat";
			VARIABLE LINE_READC: LINE;
			VARIABLE DATC : integer;
			VARIABLE CONTC:INTEGER;
			begin
			
				IF RESETN='0' THEN
					CONTC:=0;
				 ELSIF CLK' EVENT AND CLK='1' THEN

					 CONTC:=CONTC+1;
						--IF CONTC>10 AND CONTC<15 THEN
						IF CONTC > 10 AND CONTC < 15 THEN
								IF endfile( FILE_OPENEDC ) then
								ELSE
								READLINE( FILE_OPENEDC, LINE_READC);
								READ( LINE_READC, DATC );
								Checker <= to_signed(DATC ,16);
								END IF;
						--ELSif contc = 15 then
						--CONTC:=0;
						--END IF;
							
					END IF;
					END IF;		

			
END PROCESS;


Controllo: PROCESS (DATA_OUT, Checker)
BEGIN
IF (DATA_OUT = Checker) THEN
	Er2 <= '0';
ELSE
	Er2 <= '1';
END IF;
END PROCESS;

Errore <= Er1 and Er2;  -- In tal modo viene analizzato se c'� errore o meno solo per quei cicli di clock in cui la batterfly restituisce le uscite che ci interessano

BT: butterfly PORT MAP(CLK => CLK, RESETN => RESETN, DATA_IN => DATA_IN, DATA_OUT => DATA_OUT, START => START, DONE => DONE);
END STRUCTURE;