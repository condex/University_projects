-- Test Bench per la Butterfly in modolit� continua

LIBRARY IEEE;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE STD.TEXTIO.ALL;

ENTITY ButterflyContinua_TestBench IS

END ButterflyContinua_TestBench;

ARCHITECTURE structure OF ButterflyContinua_TestBench IS

COMPONENT butterfly is
port(DATA_IN: IN SIGNED(15 DOWNTO 0);
	 START,CLK,RESETN : IN STD_LOGIC;
	 DATA_OUT: OUT SIGNED(15 DOWNTO 0);
	 DONE : OUT STD_LOGIC
	);
END COMPONENT; 

SIGNAL DATA_IN, OUT_TO_IN, DATA_OUT, Checker: SIGNED (15 DOWNTO 0);
SIGNAL DONE1, DONE2, CLK, RESETN, Er1, Er2, Errore, START: STD_LOGIC;

BEGIN

CLKSYST: PROCESS
BEGIN
CLK <= '0';
WAIT FOR 50 us;
CLK <= '1';
WAIT FOR 50 us;
END PROCESS;

Reset: PROCESS
BEGIN
RESETN <= '0';
WAIT FOR 20 us;
RESETN <= '1';
WAIT;
END PROCESS;

--Analisi: PROCESS
--BEGIN
--START <= '1', '0' AFTER 1000 us;
--DATA_IN <= "0000010110000010", "1110100110011001" after 250 us, "0001000001010000" after 350 us, "1110111110001001" after 450 us, "1111000011000110" after 550 us, "1110001011011000" after 650 us;
--Checker <= "0000000001000101" after 1000 us, "1111111110001101" after 1100 us, "1111111110111011" after 1200 us, "0000000001110011" after 1300 us;
--Er1 <= '0', '1' after 1000 us, '0' after 1400 us;
--WAIT;
--END PROCESS;

LETTURADAIIN: PROCESS(CLK,RESETN)

			FILE FILE_OPENED: TEXT IS IN "DATIIN.dat";
			VARIABLE LINE_READ: LINE;
			VARIABLE DAT : integer;
			VARIABLE CONT:INTEGER;
			begin
			   
			   IF RESETN='0' THEN
					START<='0';
					CONT:=0;
				 ELSIF CLK' EVENT AND CLK='1' THEN
					CONT:=CONT+1;
					if cont = 1 then
					START<='1';
					end if;
					if cont>12 then
					  cont:=0;
			
					  else
						IF CONT>1 AND CONT<8 THEN
								IF endfile( FILE_OPENED ) then
								ELSE
								READLINE( FILE_OPENED, LINE_READ);
								READ( LINE_READ, DAT );
								DATA_IN <= to_signed(DAT ,16);
								END IF;
								IF CONT>=7 THEN
									START<='0';
								END IF;
							
					END IF;	
					END IF;	
					END IF;	
			
END PROCESS;

Controllo: PROCESS (OUT_TO_IN, Checker)
BEGIN
IF (OUT_TO_IN = Checker) THEN
	Er2 <= '0';
ELSE
	Er2 <= '1';
END IF;
END PROCESS;

Errore <= Er1 and Er2;  -- In tal modo viene analizzato se c'� errore o meno solo per quei cicli di clock in cui la batterfly restituisce le uscite che ci interessano

BT1: butterfly PORT MAP(CLK => CLK, RESETN => RESETN, DATA_IN => DATA_IN, DATA_OUT => OUT_TO_IN, START => START, DONE => DONE1);
BT2: butterfly PORT MAP(CLK => CLK, RESETN => RESETN, DATA_IN => OUT_TO_IN, DATA_OUT => DATA_OUT, START => DONE1, DONE => DONE2);

END STRUCTURE;