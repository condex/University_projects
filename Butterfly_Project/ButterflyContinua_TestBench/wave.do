onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider uPC1
add wave -noupdate -format Logic /butterflycontinua_testbench/bt1/mupc/clock
add wave -noupdate -format Logic /butterflycontinua_testbench/bt1/mupc/resetn
add wave -noupdate -format Logic /butterflycontinua_testbench/bt1/mupc/load
add wave -noupdate -format Literal /butterflycontinua_testbench/bt1/mupc/r
add wave -noupdate -format Literal /butterflycontinua_testbench/bt1/mupc/q
add wave -noupdate -divider Butterlfy
add wave -noupdate -format Logic /butterflycontinua_testbench/clk
add wave -noupdate -format Logic /butterflycontinua_testbench/resetn
add wave -noupdate -format Logic /butterflycontinua_testbench/start
add wave -noupdate -format Literal /butterflycontinua_testbench/data_in
add wave -noupdate -format Literal /butterflycontinua_testbench/out_to_in
add wave -noupdate -format Logic /butterflycontinua_testbench/done1
add wave -noupdate -format Literal /butterflycontinua_testbench/data_out
add wave -noupdate -format Logic /butterflycontinua_testbench/done2
add wave -noupdate -divider uPC2
add wave -noupdate -format Logic /butterflycontinua_testbench/bt2/mupc/clock
add wave -noupdate -format Logic /butterflycontinua_testbench/bt2/mupc/resetn
add wave -noupdate -format Logic /butterflycontinua_testbench/bt2/mupc/load
add wave -noupdate -format Literal /butterflycontinua_testbench/bt2/mupc/r
add wave -noupdate -format Literal /butterflycontinua_testbench/bt2/mupc/q
add wave -noupdate -divider {Register File 2}
add wave -noupdate -format Logic /butterflycontinua_testbench/bt2/dp/rf/clock
add wave -noupdate -format Logic /butterflycontinua_testbench/bt2/dp/rf/reset
add wave -noupdate -format Logic /butterflycontinua_testbench/bt2/dp/rf/wr_en
add wave -noupdate -format Literal /butterflycontinua_testbench/bt2/dp/rf/add_a
add wave -noupdate -format Literal /butterflycontinua_testbench/bt2/dp/rf/add_b
add wave -noupdate -format Literal /butterflycontinua_testbench/bt2/dp/rf/add_c
add wave -noupdate -format Literal /butterflycontinua_testbench/bt2/dp/rf/add_in
add wave -noupdate -format Literal /butterflycontinua_testbench/bt2/dp/rf/data_a
add wave -noupdate -format Literal /butterflycontinua_testbench/bt2/dp/rf/data_b
add wave -noupdate -format Literal /butterflycontinua_testbench/bt2/dp/rf/data_c
add wave -noupdate -format Literal /butterflycontinua_testbench/bt2/dp/rf/data_in
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
configure wave -namecolwidth 270
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 4000
configure wave -griddelta 40
configure wave -timeline 1
configure wave -timelineunits us
update
WaveRestoreZoom {0 ps} {888 ps}
