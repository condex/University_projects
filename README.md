This directory contains all projects I've done in Politecnico di Torino. All of them are open source and if you find any errors or want to give any suggestions please not hesitate to contact me.


Projects

_Flip Flop Gate Design_

    Project description
    A D flip flop gate was designed using Cadence Virtuoso. In particular, the goal was to organize transistors and via in order to respect physical and manufacturing constrains.

_VGA graphic card_

    Project description
    The aim of the project was to implemented a VGA graphic card on FPGA: the complex system was composed by an UART, VGA controller and Memory controller. All components were designed separately and then put together (divide et impera method). It was implemented in VHDL, tested with Modelsim and synthesized on FPGA using `AlteraDE2' board.

 _Butterfly Project using microprogramming tecnique and pipelined structure_

    Project description
    The purpose of this experience was to design the main building component of the classic FFT using microprogramming technique and pipelined structure.
    Moreover, a 𝝻PC, 𝝻IR and sequencer with explicit addressing have been programmed. All the project was made following precise step: Data Path, Data Flow Diagram, Control Data Flow Diagram, Timing section, Test with Modelsim. The data pattern for the test was generated using Matlab. 